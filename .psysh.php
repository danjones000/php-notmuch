<?php

$includes = [__DIR__ . '/.psysh/bootstrap.php'];

$localInclude = __DIR__ . '/.psysh/bootstrap-local.php';
if (file_exists($localInclude)) {
    $includes[] = $localInclude;
}

return [
    'defaultIncludes' => $includes,
];

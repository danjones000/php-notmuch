# php-notmuch

This library can be used as an API for accessing your [notmuch database](https://notmuchmail.org/).

This is a very early release. It's probably really buggy. You really shouldn't use it yet.

## Installation

Install with:

```shell
composer require danjones000/php-notmuch
```

However, you probably don't want to use this yet. You'll get a lot of `BadMethodCallException`s.

## Usage

```php
use Danjones\Notmuch\Notmuch;

$notmuch = new Notmuch();
// or, to specify path to notmuch command
$notmuch = new Notmuch('/path/to/notmuch.sh');
// notmuch command must be in your PATH, or an absolute path.

$notmuch->new(); // Fetches new files into database
$notmuch->search(['tag:inbox', 'AND', 'tag:unread']); // Returns an array of messages.
$notmuch->tag(['-unread'], ['tag:unread']); // Untag all unread messages as unread
// ...
```

The API closely models the CLI usage.

## Future development plans.

In the future, a more object-oriented approach will be added. I'd like to be able to do something like:

```php
$messages = $notmuch->search(['tag:inbox']); // $messages will be a MessageCollection
$messages->tag(['+newtag']);
$first = $messages->first(); // $first will be a Message object
$first->tag(['+anothertag']);
```

## Development

[Merge requests](https://gitlab.com/danjones000/php-notmuch/-/merge_requests) are welcome.

Code should be PSR-12 compliant.

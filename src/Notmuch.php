<?php

declare(strict_types=1);

namespace Danjones\Notmuch;

use InvalidArgumentException;
use Symfony\Component\Process\Process;

/**
 * Interface to command-line notmuch application.
 *
 * @todo error handling
 */
class Notmuch
{
    /** @var string path to notmuch command */
    protected $path = 'notmuch';

    /**
     * Constructor.
     */
    public function __construct(string $path = 'notmuch')
    {
        $this->path = $path;
        $this->ensureValidPath();
    }

    /**
     * Ensures that $this->path is a valid command
     *
     * @todo
     */
    protected function ensureValidPath(): void
    {
        // @todo
    }

    /**
     * Ensures that res is a readable stream
     *
     * @param resource $res
     * @throws InvalidArgumentException if $res is not a readable stream
     */
    protected function ensureReadableStream($res): void
    {
        if (gettype($res) !== 'resource') {
            // We'll let symfony/process check everything else
            return;
        }

        if (($type = get_resource_type($res)) !== 'stream') {
            throw new InvalidArgumentException("Resource must be a stream. $type passed.");
        }

        $mode = stream_get_meta_data($res)['mode'] ?? 'x';
        if ($mode[0] !== 'r') {
            throw new InvalidArgumentException('Resource must be a readable stream.');
        }
    }

    /**
     * Runs a notmuch command.
     *
     * This method will not close resources, so it must be done manually.
     * If you only need stdout, use self::getCommandOutput instead.
     *
     * @param string $command
     * @param string[] $arguments arguments passed after $command
     * @param resource|null $stdin If supplied, will be used for stdin to the command
     * @return Process
     * @see self::getCommandOutput
     */
    public function runCommand(string $command, array $arguments = [], $stdin = null): Process
    {
        $args = [];
        $args[] = $this->path;
        $args[] = $command;
        array_push($args, ...$arguments);

        $process = new Process($args);

        if ($stdin) {
            $this->ensureReadableStream($stdin);
            $process->setInput($stdin);
        }

        $process->start();

        return $process;
    }

    /**
     * Runs a notmuch command and returns stdout.
     *
     * This method will properly close resources before returning stdout.
     *
     * @param string $command
     * @param string[] $arguments arguments passed after $command
     * @param resource $stdin If supplied, will be used for stdin to the command
     * @return string
     */
    public function getCommandOutput(string $command, array $arguments = [], $stdin = null): string
    {
        $proc = $this->runCommand($command, $arguments, $stdin);
        $proc->wait();

        return $proc->getOutput();
    }

    /**
     * Gets the count of messages.
     *
     * $options may include:
     *   [
     *     'output' => 'messages'|'threads'|'files'
     *     'exclude' => true|false
     *   ]
     *
     * @param array|null $search If supplied, will return count for this search
     * @param string[] $options
     * @return int
     * @todo Allow for batch operations
     */
    public function count(array $search = null, array $options = []): int
    {
        $args = [];
        foreach ($options as $key => $value) {
            switch ($key) {
                case 'output':
                    $args[] = "--{$key}={$value}";
                    break;
                case 'exclude':
                    $args[] = '--exclude=' . json_encode(boolval($value));
                    break;
            }
        }
        if ($search) {
            $args = [...$args, '--', ...$search];
        }

        return intval(trim($this->getCommandOutput('count', $args)));
    }

    /**
     * Searches for messages.
     *
     * $options may include:
     *   [
     *     'output' => 'summary'|'messages'|'threads'|'files'|'tags'
     *     'exclude' => true|false|'all'|'flag'
     *     'sort' => 'oldest-first'|'newest-first'
     *     'offset' => int
     *     'limit' => int
     *     'duplicate' => int
     *   ]
     *
     * @param string[] $search array of search terms
     * @param string[] $options
     * @return array <phpdoctor-ignore-this-line/>
     */
    public function search(array $search = [], array $options = []): array
    {
        if (empty($search)) {
            throw new InvalidArgumentException('notmuch search requires at least one search term');
        }

        $args = ['format' => 'json', 'output' => 'summary'];
        foreach ($options as $key => $value) {
            switch ($key) {
                case 'output':
                    switch ($value) {
                        case 'summary':
                            $args['format'] = 'json';
                            $args['output'] = 'summary';
                            break;
                        case 'threads':
                        case 'messages':
                        case 'files':
                        case 'tags':
                            $args['format'] = 'text0';
                            $args['output'] = $value;
                    }
                    break;
                case 'exclude':
                    $value = is_bool($value) ? json_encode($value) : $value;
                    // fall-through
                case 'sort':
                case 'offset':
                case 'limit':
                case 'duplicate':
                    $args[$key] = $value;
                    break;
            }
        }

        $cliArgs = [];
        foreach ($args as $key => $value) {
            $cliArgs[] = "--{$key}={$value}";
        }

        $out = trim($this->getCommandOutput('search', [...$cliArgs, '--', ...$search]));

        return $args['format'] == 'json' ? (json_decode($out, true) ?: []) : explode("\0", $out);
    }

    /**
     * Gets all parts of a message.
     *
     * @param string $id
     * @return array <phpdoctor-ignore-this-line/>
     */
    public function getMessageParts(string $id): array
    {
        $id = preg_replace('/^id:/', '', $id);
        $emails = json_decode($this->getCommandOutput('show', ['--format=json', '--', "id:$id"]), true);
        $email = reset($emails);
        $parts = reset($email);
        $main = $parts[0];
        // $children = $parts[1]; // We don't need this here

        $ret = [0 => $main];
        $body = $main['body'][0] ?? [];
        $this->processMessageParts($ret, $body);

        return $ret;
    }

    /**
     * Recursively go through each message part, and add to array
     *
     * @param string[] $ret
     * @param string[] $part
     */
    protected function processMessageParts(array &$ret, array $part): void
    {
        $ret[$part['id']] = $part;
        if (is_array($part['content'] ?? null)) {
            foreach ($part['content'] as $subPart) {
                $this->processMessageParts($ret, $subPart);
            }
        }
    }

    /**
     * Gets the raw body of a single message part.
     *
     * @param string $id The id of the message
     * @param int $part the id of the part, or 0 for the whole message
     * @param bool $embedAttachments If true, html parts will have cid:<id> uris replaced with data: uris
     * @return string
     * @todo figure out why this hangs sometimes
     */
    public function getMessagePart(string $id, int $part, bool $embedAttachments = false): string
    {
        $id = preg_replace('/^id:/', '', $id);

        $out = $this->getCommandOutput('show', ['--format=raw', "--part=$part", '--', "id:$id"]);
        if ($embedAttachments) {
            $parts = $this->getMessageParts($id);
            $replacements = [];
            foreach ($parts as $partNum => $part) {
                if ($cid = $part['content-id'] ?? null) {
                    $replacements["cid:$cid"] = sprintf(
                        'data:%sbase64,%s',
                        ($mime = $part['content-type'] ?? null) ? $mime . ';' : '',
                        base64_encode($this->getMessagePart($id, $partNum, false))
                    );
                }
            }

            $out = str_replace(array_keys($replacements), array_values($replacements), $out);
        }

        return $out;
    }

    /**
     * Gets a raw email message.
     *
     * @param string $id The id of the message
     * @return string
     */
    public function getRawMessage(string $id): string
    {
        return $this->getMessagePart($id, 0, false);
    }

    /**
     * Returns an array of messages.
     *
     * Often used for fetching a thread.
     *
     * @param string[] $search
     * @param string[] $options
     * @return array <phpdoctor-ignore-this-line/>
     */
    public function show(array $search, array $options = []): array
    {
        $this->throwNotImplemented();
    }

    /**
     * Reads new messages.
     *
     * @return string
     * @todo add option parsing
     */
    public function new(): string
    {
        return trim($this->getCommandOutput('new'));
    }

    /**
     * Inserts a new message into the database.
     *
     * @param resource $stream
     * @param string[] $tags
     * @param string[] $options
     * @return bool If the insert was successful
     */
    public function insert($stream, array $tags = [], array $options = []): bool
    {
        $this->throwNotImplemented();
    }

    /**
     * Search for known email addresses from a given search.
     *
     * The returned array will be an array of arrays, in which each item contains name, address, and name-addr.
     *
     * @param string[] $search
     * @param string[] $options
     * @return string[]
     */
    public function address(array $search, array $options): array
    {
        $this->throwNotImplemented();
    }

    /**
     * Tags messages.
     *
     * @param string[] $tags
     * @param string[] $search
     * @param string[] $options
     * @return bool if successful
     */
    public function tag(array $tags, array $search, array $options = []): bool
    {
        $args = [];
        $stdin = null;

        foreach ($options as $key => $value) {
            switch ($key) {
                case 'remove-all':
                case 'batch':
                    if ($value) {
                        $args[] = "--{$key}}";
                    }
                    break;
                case 'input':
                    if (is_string($value) && file_exists($value)) {
                        $args[] = "--{$key}={$value}";
                    } else {
                        $stdin = $value;
                    }
                    break;
            }
        }

        foreach ($tags as $tag) {
            $tag = trim($tag);
            if (empty($tag)) {
                continue;
            }

            $pm = $tag[0];
            if ($pm !== '+' && $pm !== '-') {
                $tag = '+' . $tag;
            }

            $args[] = $tag;
        }

        $args[] = '--';

        array_push($args, ...$search);

        $proc = $this->runCommand('tag', $args, $stdin);

        return $proc->wait() === 0;
    }

    /**
     * Returns an iterable object that can be used to set/get config values.
     *
     * @return iterable
     */
    public function config(): iterable
    {
        $this->throwNotImplemented();
    }

    /**
     * This can be used for methods being stubbed out.
     *
     * @throws BadMethodCallException
     */
    protected function throwNotImplemented(): void
    {
        $back = debug_backtrace();
        $caller = $back[1] ?? [];
        $class = $caller['class'] ?? null;
        $method = $caller['function'] ?? '';
        $method = $class ? $class . '::' . $method : $method;

        throw new \BadMethodCallException("$method is not yet implemented");
    }
}
